import sys
import os

def isWhiteLine(string):
    if '/t' in string or ' ' in string:
        return True
    else:
        return False


file = sys.argv[1]

if os.path.exists(file):
    with open(file) as data:
        for item in data.readlines():
            flag = isWhiteLine(item)
            if flag == False:
                print item
else:
    print "File doesn't exist in the given path"
