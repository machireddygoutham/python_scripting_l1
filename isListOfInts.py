
def isListOfInts(data):

    if type(data) is list:
        if not data:
            return False
        else:
            for item in data:
                if type(item) is not int:
                    return False
            return True
    else: raise ValueError(str(data) + "- arg not of <list> type")


def testList(cont):
    print isListOfInts(cont)

testList([])
testList([1])
testList([1,2])
testList([0])
testList(['1'])
testList([1,'a'])
testList(['a',1])
testList([1, 1.])
testList([1., 1.])
testList((1,2))